﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using OfficeOpenXml;
using RestSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Windows.Forms;

namespace BGCA
{
    public partial class Form1 : Form
    {
        private string AccessToken = "";
        private string asiBaseAddress = "";
        JArray FinalFinancialEntity_array = new JArray();
        JArray FinalGLAccount_array = new JArray();
        JArray FinalTaxCategory_array = new JArray();
        JArray FinalGroup_array = new JArray();
        JArray FinalItemClass_array = new JArray();

        public Form1()
        {
            InitializeComponent();
            openFileDialog1.Filter = "Excel files (*.xlsx)|*.xlsx";
            openFileDialog1.InitialDirectory = "C://Desktop";
            openFileDialog1.Title = "Select excel file to be upload.";
            openFileDialog1.FilterIndex = 1;

            asiBaseAddress = System.Configuration.ConfigurationManager.AppSettings["ASIBaseAddress"].ToString();
            lblAsibaseAddress.Text = asiBaseAddress;

            GenerateToken();
        }

        private void GenerateToken()
        {
            try
            {
                //string asiBaseAddress = System.Configuration.ConfigurationManager.AppSettings["ASIBaseAddress"].ToString();
                string asiUserName = System.Configuration.ConfigurationManager.AppSettings["ASIusername"].ToString();
                string asiPassword = System.Configuration.ConfigurationManager.AppSettings["ASIpassword"].ToString();
                string asiGrantType = System.Configuration.ConfigurationManager.AppSettings["ASIgrant_type"].ToString();

                var client = new HttpClient();
                client.BaseAddress = new Uri(asiBaseAddress);
                var request = new HttpRequestMessage(HttpMethod.Post, "token");

                var keyValues = new List<KeyValuePair<string, string>>();
                keyValues.Add(new KeyValuePair<string, string>("username", asiUserName));
                keyValues.Add(new KeyValuePair<string, string>("password", asiPassword));
                keyValues.Add(new KeyValuePair<string, string>("grant_type", asiGrantType));

                request.Content = new FormUrlEncodedContent(keyValues);
                var responseMessage = client.SendAsync(request);
                var responseJSONData = responseMessage.Result.Content.ReadAsStringAsync().Result;

                var Replace_responseJSONData = responseJSONData.Replace(".issued", "issued").Replace(".expires", "expires");

                TokenResponse tokenResponse = JsonConvert.DeserializeObject<TokenResponse>(Replace_responseJSONData);

                // access token is not null and error not occured occured
                if (string.IsNullOrWhiteSpace(tokenResponse.error) && !string.IsNullOrWhiteSpace(tokenResponse.access_token))
                    AccessToken = tokenResponse.access_token;
                else
                    txtResponse.Text = "Error occured while generating token: " + tokenResponse.error + "\r\n";
            }
            catch (Exception ex)
            {
                txtResponse.AppendText("Error occured while generating token: " + "\r\n");

                if (!string.IsNullOrEmpty(ex.Message))
                    txtResponse.AppendText(ex.Message + "\r\n");
                if (ex.InnerException != null)
                {
                    if (!string.IsNullOrEmpty(ex.InnerException.Message))
                        txtResponse.AppendText("\nInner exception: " + ex.InnerException.Message + "\r\n");
                }
            }
        }
        //--------------------------------------------------------------------------//
        private void btnBrowseFile_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(AccessToken))
                    MessageBox.Show("Access token not generated");
                else
                {
                    if (openFileDialog1.ShowDialog() == DialogResult.OK)
                    {
                        if (openFileDialog1.CheckFileExists)
                        {
                            string FullPath = Path.GetFullPath(openFileDialog1.FileName);
                            string Extension = Path.GetExtension(openFileDialog1.FileName);

                            if (Extension.ToLower() == ".xlsx")
                                txtFileName.Text = FullPath;
                            else
                            {
                                openFileDialog1.FileName = null;
                                txtFileName.Text = null;
                                MessageBox.Show("Please upload .xlsx (excel) file only");
                            }
                        }
                    }
                    //else
                    //{
                    //    openFileDialog1.FileName = null;
                    //    txtFileName.Text = null;
                    //    MessageBox.Show("Please Upload document.");
                    //}
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        public void ClearTextBox()
        {
            txtResponse.Text = string.Empty;
            txtExcelRowError.Text = string.Empty;

            txtResponse.Refresh();
            txtExcelRowError.Refresh();
        }

        private void btnSubmitFile_Click(object sender, EventArgs e)
        {
            ClearTextBox();

            string FullFilePath = string.Empty;
            try
            {
                txtResponse.AppendText("Importing Simple Products. Please wait... \r\n");

                if (string.IsNullOrEmpty(AccessToken))
                    MessageBox.Show("Access token not generated");
                else
                {
                    string Filename = Path.GetFileName(openFileDialog1.FileName);

                    if (string.IsNullOrWhiteSpace(Filename))
                        MessageBox.Show("Please select a valid document.");
                    else
                    {
                        // Make Final array empty
                        FinalFinancialEntity_array = new JArray();
                        FinalGLAccount_array = new JArray();
                        FinalTaxCategory_array = new JArray();
                        FinalGroup_array = new JArray();

                        // Call method                        
                        GetFinancialEntity();
                        GetGLAccount();
                        GetTaxCategory();
                        GetItemClass();
                        GetGroup();

                        if (FinalFinancialEntity_array != null && FinalGLAccount_array != null
                            && FinalTaxCategory_array != null && FinalGroup_array != null
                            && FinalItemClass_array != null)
                        {
                            if (FinalFinancialEntity_array.Count > 0 && FinalGLAccount_array.Count > 0
                            && FinalTaxCategory_array.Count > 0 && FinalGroup_array.Count > 0
                            && FinalItemClass_array.Count > 0)
                            {
                                #region Save Excel file
                                string FolderPath = Application.StartupPath + "\\Document\\";

                                if (!Directory.Exists(FolderPath))
                                    Directory.CreateDirectory(FolderPath);

                                Filename = Guid.NewGuid().ToString() + "_" + DateTime.Now.Date.ToString("yyyy_MM_dd") + ".xlsx";

                                FullFilePath = FolderPath + Filename;

                                File.Copy(openFileDialog1.FileName, FullFilePath);
                                #endregion

                                // once file save read all rows
                                FileInfo ReadExcelFile = new FileInfo(FullFilePath);

                                using (ExcelPackage package = new ExcelPackage(ReadExcelFile))
                                {
                                    ExcelWorksheet workSheet = package.Workbook.Worksheets["Sheet1"];
                                    int TotalRows = workSheet.Dimension.Rows;

                                    if (TotalRows <= 1)
                                        MessageBox.Show("No data found in excel to process");
                                    else
                                    {
                                        JObject jobj = null;

                                        // read JSON directly from a file                                
                                        using (StreamReader file = File.OpenText(Application.StartupPath + "\\samplejson1.json"))
                                        {
                                            using (JsonTextReader reader = new JsonTextReader(file))
                                            {
                                                jobj = (JObject)JToken.ReadFrom(reader);
                                            }
                                        }
                                        if (jobj != null)
                                        {
                                            //JArray jArray = new JArray();                                            

                                            for (int i = 2; i <= TotalRows; i++)
                                            {
                                                try
                                                {
                                                    JObject tmpjObject = new JObject(jobj);
                                                    Guid ItemIdobj = Guid.NewGuid();

                                                    #region Read Excel Data Cell

                                                    string Name = Convert.ToString(workSheet.Cells[i, 1].Value);
                                                    string ProductCode = Convert.ToString(workSheet.Cells[i, 2].Value);

                                                    string EffectiveDate = Convert.ToString(workSheet.Cells[i, 3].Value);
                                                    string EndingDate = Convert.ToString(workSheet.Cells[i, 4].Value);

                                                    string PriceStandard = Convert.ToString(workSheet.Cells[i, 5].Value);
                                                    string FinancialEntity = Convert.ToString(workSheet.Cells[i, 6].Value);

                                                    string AccountsReceivable = Convert.ToString(workSheet.Cells[i, 7].Value);
                                                    string AccountsName = Convert.ToString(workSheet.Cells[i, 8].Value);

                                                    string Income = Convert.ToString(workSheet.Cells[i, 9].Value);
                                                    string IncomeName = Convert.ToString(workSheet.Cells[i, 10].Value);

                                                    string Description = Convert.ToString(workSheet.Cells[i, 11].Value);
                                                    string TaxCategory = Convert.ToString(workSheet.Cells[i, 12].Value);
                                                    string AccountingMethod = Convert.ToString(workSheet.Cells[i, 13].Value);

                                                    /* New field add on 2020-05-21 */
                                                    string AssignPurchaserToGroup = Convert.ToString(workSheet.Cells[i, 14].Value);
                                                    string GroupName = Convert.ToString(workSheet.Cells[i, 15].Value);
                                                    string GroupRole = Convert.ToString(workSheet.Cells[i, 16].Value);
                                                    string TermStartDate = Convert.ToString(workSheet.Cells[i, 17].Value);
                                                    string TermEndDate = Convert.ToString(workSheet.Cells[i, 18].Value);
                                                    string TermSpanInMonth = Convert.ToString(workSheet.Cells[i, 19].Value);

                                                    bool Handling = Convert.ToBoolean(workSheet.Cells[i, 20].Value);
                                                    string ItemClassName = Convert.ToString(workSheet.Cells[i, 21].Value);

                                                    #endregion

                                                    #region Set values in Jobject

                                                    /*-------------------Set 1.Group, 2. Group-> GroupClass, 3.GroupRole, 4.GroupTermPolicy add on 2020-05-21 -----------------------------*/

                                                    if (!string.IsNullOrWhiteSpace(AssignPurchaserToGroup) && Convert.ToBoolean(AssignPurchaserToGroup) == true)
                                                    {
                                                        // Set GroupTermPolicy
                                                        try
                                                        {
                                                            tmpjObject["GroupTermPolicy"]["TermBeginDate"] = Convert.ToDateTime(TermStartDate).ToString("yyyy-MM-dd");
                                                            tmpjObject["GroupTermPolicy"]["TermEndDate"] = Convert.ToDateTime(TermEndDate).ToString("yyyy-MM-dd");
                                                        }
                                                        catch (Exception ex)
                                                        {
                                                        }

                                                        if (int.TryParse(TermSpanInMonth, out int t_TermSpanInMonth))
                                                            tmpjObject["GroupTermPolicy"]["TermSpan"] = TermSpanInMonth;

                                                        var temp_group_array = FinalGroup_array.Where(x => x["Name"]?.ToString() == GroupName).FirstOrDefault();

                                                        if (temp_group_array != null)
                                                        {
                                                            // Set Group
                                                            string GroupId = Convert.ToString(temp_group_array["GroupId"]);
                                                            string GroupDescription = Convert.ToString(temp_group_array["Description"]);

                                                            tmpjObject["Group"]["GroupId"] = GroupId;
                                                            tmpjObject["Group"]["Name"] = GroupName;
                                                            tmpjObject["Group"]["Description"] = GroupDescription;

                                                            // Set Group -> GroupClass
                                                            string GroupClass_GroupClassId = Convert.ToString(temp_group_array["GroupClass"]["GroupClassId"]);
                                                            string GroupClass_Name = Convert.ToString(temp_group_array["GroupClass"]["Name"]);
                                                            string GroupClass_Description = Convert.ToString(temp_group_array["GroupClass"]["Description"]);

                                                            tmpjObject["Group"]["GroupClass"]["GroupClassId"] = GroupClass_GroupClassId;
                                                            tmpjObject["Group"]["GroupClass"]["Name"] = GroupClass_Name;
                                                            tmpjObject["Group"]["GroupClass"]["Description"] = GroupClass_Description;

                                                            // Set GroupRoles Values
                                                            var temp_group_array_role = temp_group_array["Roles"]["$values"];

                                                            var temp_group_array_role_value = temp_group_array_role.Where(x => x["Name"]?.ToString().ToLower() == GroupRole.ToLower()).FirstOrDefault();

                                                            if (temp_group_array_role_value != null)
                                                            {
                                                                string GroupRole_RoleId = Convert.ToString(temp_group_array_role_value["RoleId"]);
                                                                string GroupRole_Description = Convert.ToString(temp_group_array_role_value["Description"]);

                                                                tmpjObject["GroupRole"]["RoleId"] = GroupRole_RoleId;
                                                                tmpjObject["GroupRole"]["Description"] = GroupRole_Description;
                                                                tmpjObject["GroupRole"]["Name"] = GroupRole;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            // Remove Group, GroupRole, GroupTermPolicy
                                                            var t = (JObject)tmpjObject.SelectToken("");
                                                            t.Property("Group").Remove();

                                                            t = (JObject)tmpjObject.SelectToken("");
                                                            t.Property("GroupRole").Remove();

                                                            t = (JObject)tmpjObject.SelectToken("");
                                                            t.Property("GroupTermPolicy").Remove();
                                                        }
                                                    }

                                                    else
                                                    {
                                                        // Remove Group, GroupRole, GroupTermPolicy
                                                        var t = (JObject)tmpjObject.SelectToken("");
                                                        t.Property("Group").Remove();

                                                        t = (JObject)tmpjObject.SelectToken("");
                                                        t.Property("GroupRole").Remove();

                                                        t = (JObject)tmpjObject.SelectToken("");
                                                        t.Property("GroupTermPolicy").Remove();
                                                    }

                                                    // ------------------------- Set tax category -------------------------

                                                    string TaxCategoryGuid = string.Empty;
                                                    bool isTaxcategoryMatch = false;

                                                    if (string.IsNullOrWhiteSpace(TaxCategory) == false)
                                                    {
                                                        foreach (var items in FinalTaxCategory_array)
                                                        {
                                                            foreach (var item in items)
                                                            {
                                                                if (item["Name"]?.ToString().ToLower() == "TaxCategoryKey".ToLower())
                                                                {
                                                                    TaxCategoryGuid = Convert.ToString(item["Value"]["$value"]);
                                                                }
                                                                if (item["Value"]?.ToString().ToLower() == TaxCategory.ToLower())
                                                                {
                                                                    isTaxcategoryMatch = true;
                                                                    break;
                                                                }
                                                            }
                                                            if (isTaxcategoryMatch)
                                                            {
                                                                break;
                                                            }
                                                        }
                                                    }

                                                    if (isTaxcategoryMatch)
                                                    {
                                                        // Set taxcategory guid
                                                        tmpjObject["ItemFinancialInformation"]["TaxCategory"]["TaxCategoryId"] = TaxCategoryGuid;
                                                    }
                                                    else
                                                    {
                                                        // remove ItemFinancialAccounts -> AccountsReceivable
                                                        var t = (JObject)tmpjObject.SelectToken("ItemFinancialInformation");
                                                        t.Property("TaxCategory").Remove();
                                                    }

                                                    // ------------------------- Set accounting method -------------------------

                                                    if (string.IsNullOrWhiteSpace(AccountingMethod) == false)
                                                    {
                                                        if (AccountingMethod.ToLower() == "cash")
                                                        {
                                                            // remove ItemFinancialInformation -> AccountingMethod
                                                            var t = (JObject)tmpjObject.SelectToken("ItemFinancialInformation");
                                                            t.Property("AccountingMethod").Remove();
                                                        }
                                                    }

                                                    if (string.IsNullOrWhiteSpace(AccountsReceivable) == false && string.IsNullOrWhiteSpace(AccountsName) == false)
                                                    {
                                                        var temp_glaccount_array = FinalGLAccount_array.Where(x => x["GLAccountCode"]?.ToString().ToLower() == AccountsReceivable.ToLower()
                                                        && x["Name"]?.ToString().ToLower() == AccountsName.ToLower()).FirstOrDefault();

                                                        if (temp_glaccount_array != null)
                                                        {
                                                            //string GLAccountName = Convert.ToString(temp_glaccount_array["Name"]);
                                                            string GLAccountId = Convert.ToString(temp_glaccount_array["GLAccountId"]);

                                                            tmpjObject["ItemFinancialInformation"]["ItemFinancialAccounts"]["AccountsReceivable"]["GLAccount"]["GLAccountId"] = GLAccountId;
                                                            tmpjObject["ItemFinancialInformation"]["ItemFinancialAccounts"]["AccountsReceivable"]["GLAccount"]["GLAccountCode"] = AccountsReceivable;
                                                            tmpjObject["ItemFinancialInformation"]["ItemFinancialAccounts"]["AccountsReceivable"]["GLAccount"]["Name"] = AccountsName;
                                                        }
                                                        else
                                                        {
                                                            // remove ItemFinancialAccounts -> AccountsReceivable
                                                            var t = (JObject)tmpjObject.SelectToken("ItemFinancialInformation.ItemFinancialAccounts");
                                                            t.Property("AccountsReceivable").Remove();
                                                        }
                                                    }
                                                    else
                                                    {
                                                        // remove ItemFinancialAccounts -> AccountsReceivable
                                                        var t = (JObject)tmpjObject.SelectToken("ItemFinancialInformation.ItemFinancialAccounts");
                                                        t.Property("AccountsReceivable").Remove();
                                                    }

                                                    // ---------------------------------------------------------------------------

                                                    if (string.IsNullOrWhiteSpace(Income) == false && string.IsNullOrWhiteSpace(IncomeName) == false)
                                                    {
                                                        var temp_glaccount_array = FinalGLAccount_array.Where(x => x["GLAccountCode"]?.ToString().ToLower() == Income.ToLower()
                                                        && x["Name"]?.ToString().ToLower() == IncomeName.ToLower()).FirstOrDefault();

                                                        if (temp_glaccount_array != null)
                                                        {
                                                            //string GLAccountName = Convert.ToString(temp_glaccount_array["Name"]);
                                                            string GLAccountId = Convert.ToString(temp_glaccount_array["GLAccountId"]);

                                                            tmpjObject["ItemFinancialInformation"]["ItemFinancialAccounts"]["Income"]["GLAccount"]["GLAccountId"] = GLAccountId;
                                                            tmpjObject["ItemFinancialInformation"]["ItemFinancialAccounts"]["Income"]["GLAccount"]["GLAccountCode"] = Income;
                                                            tmpjObject["ItemFinancialInformation"]["ItemFinancialAccounts"]["Income"]["GLAccount"]["Name"] = IncomeName;
                                                        }
                                                        else
                                                        {
                                                            // remove ItemFinancialAccounts -> Income
                                                            var t = (JObject)tmpjObject.SelectToken("ItemFinancialInformation.ItemFinancialAccounts");
                                                            t.Property("Income").Remove();
                                                        }
                                                    }
                                                    else
                                                    {
                                                        // remove ItemFinancialAccounts -> Income         
                                                        var t = (JObject)tmpjObject.SelectToken("ItemFinancialInformation.ItemFinancialAccounts");
                                                        t.Property("Income").Remove();
                                                    }

                                                    if (string.IsNullOrWhiteSpace(FinancialEntity) == false)
                                                    {
                                                        var financialentity_token = FinalFinancialEntity_array.Where(x => x["EntityCode"]?.ToString().ToLower() == FinancialEntity.ToLower()
                                                        && x["IsDefault"]?.ToString().ToLower() == "True".ToLower()).FirstOrDefault();

                                                        if (financialentity_token != null)
                                                        {
                                                            string FinancialEntityId = Convert.ToString(financialentity_token["FinancialEntityId"]);
                                                            tmpjObject["ItemFinancialInformation"]["FinancialEntity"]["FinancialEntityId"] = FinancialEntityId;
                                                        }
                                                    }

                                                    if (Handling == false)
                                                    {
                                                        // remove IncursHandling
                                                        var t = (JObject)tmpjObject.SelectToken("ItemFinancialInformation");
                                                        t.Property("IncursHandling").Remove();
                                                    }

                                                    // -------------------------------Item Class-------------------------------------------

                                                    if (!string.IsNullOrWhiteSpace(ItemClassName))
                                                    {
                                                        // Set Item class value
                                                        var temp_itemclass_array = FinalItemClass_array.Where(x => x["Name"]?.ToString().ToLower() == ItemClassName.ToLower()).FirstOrDefault();

                                                        if (temp_itemclass_array != null)
                                                        {
                                                            string ItemClassId = Convert.ToString(temp_itemclass_array["ItemClassId"]);

                                                            tmpjObject["ItemClass"]["ItemClassId"] = ItemClassId;
                                                            tmpjObject["ItemClass"]["Name"] = ItemClassName;
                                                        }
                                                        else
                                                        {
                                                            // Remove ItemClass
                                                            var t = (JObject)tmpjObject.SelectToken("");
                                                            t.Property("ItemClass").Remove();
                                                        }
                                                    }
                                                    else
                                                    {
                                                        // Remove ItemClass
                                                        var t = (JObject)tmpjObject.SelectToken("");
                                                        t.Property("ItemClass").Remove();
                                                    }


                                                    // ---------------------------------------------------------------------------

                                                    tmpjObject["Name"] = Name;
                                                    tmpjObject["ItemCode"] = ProductCode;
                                                    tmpjObject["Description"] = Description;
                                                    tmpjObject["PublishingInformation"]["StartDate"] = EffectiveDate;
                                                    tmpjObject["PublishingInformation"]["ExpirationDate"] = EndingDate;

                                                    tmpjObject["ItemId"] = ItemIdobj.ToString();

                                                    if (decimal.TryParse(PriceStandard, out decimal t_Price))
                                                        tmpjObject["TempDefaultPrice"] = t_Price;

                                                    #endregion

                                                    #region Api call
                                                    string ItemData = Newtonsoft.Json.JsonConvert.SerializeObject(tmpjObject);

                                                    if (!string.IsNullOrWhiteSpace(ItemData))
                                                    {
                                                        // Call Api
                                                        string ProductItem_API_Url = asiBaseAddress + "/api/productitem";

                                                        var client = new RestClient(ProductItem_API_Url);
                                                        var request = new RestRequest(Method.POST);

                                                        request.AddHeader("cache-control", "no-cache");
                                                        request.AddHeader("Content-Type", "application/json");
                                                        request.AddHeader("Authorization", "Bearer " + AccessToken);

                                                        request.AddParameter("undefined", ItemData, ParameterType.RequestBody);
                                                        IRestResponse response = client.Execute(request);

                                                        if (response.StatusCode == System.Net.HttpStatusCode.Created || response.StatusCode == System.Net.HttpStatusCode.OK)
                                                            txtResponse.AppendText("Row " + i + " imported" + "\r\n");
                                                        else
                                                            txtExcelRowError.AppendText("Error in Row " + i + ": " + response.Content + "\r\n");
                                                    }
                                                    else
                                                        txtExcelRowError.AppendText("Error in Row " + i + ": No data found to post" + "\r\n");
                                                    #endregion

                                                    //jArray.Add(tmpjObject);
                                                }
                                                catch (Exception ex)
                                                {
                                                    txtExcelRowError.AppendText("Error in Row  " + i + ": " + ex.Message + "\r\n");
                                                }
                                            }

                                            #region Rebuild index                                            
                                            JObject taskdefination_jobj = null;

                                            // read JSON directly from a file
                                            using (StreamReader file = File.OpenText(Application.StartupPath + "\\taskdefination.json"))
                                            {
                                                using (JsonTextReader reader = new JsonTextReader(file))
                                                {
                                                    taskdefination_jobj = (JObject)JToken.ReadFrom(reader);
                                                }
                                            }

                                            if (taskdefination_jobj != null)
                                            {
                                                string TaskDefinatonRequestData = Newtonsoft.Json.JsonConvert.SerializeObject(taskdefination_jobj);

                                                string TaskDefinationApiUrl = asiBaseAddress + "/api/taskdefinition";
                                                var taskdefinition_client = new RestClient(TaskDefinationApiUrl);
                                                var taskdefinition_request = new RestRequest(Method.POST);

                                                taskdefinition_request.AddHeader("cache-control", "no-cache");
                                                taskdefinition_request.AddHeader("Content-Type", "application/json");
                                                taskdefinition_request.AddHeader("Authorization", "Bearer " + AccessToken);

                                                taskdefinition_request.AddParameter("undefined", TaskDefinatonRequestData, ParameterType.RequestBody);

                                                IRestResponse taskdefinition_response = taskdefinition_client.Execute(taskdefinition_request);

                                                if (taskdefinition_response.StatusCode == System.Net.HttpStatusCode.Created
                                                    || taskdefinition_response.StatusCode == System.Net.HttpStatusCode.OK)
                                                {
                                                    // get taskdefinationId.
                                                    JValue taskdefinition_value = (JValue)taskdefinition_response.Content;
                                                    var taskdefinition_obj = JObject.Parse(taskdefinition_value.Value.ToString());

                                                    string TaskDefinationId = Convert.ToString(taskdefinition_obj["TaskDefinitionId"]);

                                                    // Call TaskDefinition/_Execute
                                                    string TaskDefinationExecuteApiUrl = asiBaseAddress + "/api/TaskDefinition/_Execute";

                                                    var taskdefinition_execute_client = new RestClient(TaskDefinationExecuteApiUrl);
                                                    var taskdefinition_execute_request = new RestRequest(Method.POST);

                                                    taskdefinition_execute_request.AddHeader("cache-control", "no-cache");
                                                    taskdefinition_execute_request.AddHeader("Content-Type", "application/json");
                                                    taskdefinition_execute_request.AddHeader("Authorization", "Bearer " + AccessToken);

                                                    string TaskdefinationExecuteJson = "{'$type': 'Asi.Soa.Communications.DataContracts.TaskRequest," +
                                                        "Asi.Contracts','TaskDefinitionId': '" + TaskDefinationId + "'," +
                                                        "'EntityTypeName': 'TaskDefinition','OperationName': 'TaskAdhocRequest'}";

                                                    taskdefinition_execute_request.AddParameter("undefined", TaskdefinationExecuteJson, ParameterType.RequestBody);

                                                    IRestResponse taskdefinition_execute_response = taskdefinition_execute_client.Execute(taskdefinition_execute_request);
                                                }
                                                else
                                                    txtExcelRowError.AppendText("Task defination api call error :" + taskdefinition_response.Content + "\r\n");
                                            }
                                            else
                                                MessageBox.Show("Unable to read TaskDefination json file for rebuild index");
                                            #endregion
                                        }
                                        else
                                            MessageBox.Show("Unable to read SampleJson files");
                                    }
                                }
                            }
                            else
                            {
                                if (FinalFinancialEntity_array.Count == 0)
                                    txtResponse.AppendText("Finalcial entity data not found \r\n");
                                if (FinalGLAccount_array.Count == 0)
                                    txtResponse.AppendText("GL account data not found \r\n");
                                if (FinalTaxCategory_array.Count == 0)
                                    txtResponse.AppendText("Tax category data not found \r\n");
                                if (FinalGroup_array.Count == 0)
                                    txtResponse.AppendText("Group data not found \r\n");
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                txtExcelRowError.AppendText("Message: " + ex.Message + "\r\n");

                if (ex.InnerException != null)
                    txtExcelRowError.AppendText("Inner Exception: " + ex.InnerException.Message);
            }

            // At Last Delete File
            if (!string.IsNullOrEmpty(FullFilePath))
            {
                if (File.Exists(FullFilePath))
                {
                    GC.WaitForFullGCComplete();
                    GC.Collect();
                    File.Delete(FullFilePath);
                }

            }

            txtResponse.AppendText("Simple Products imported from Excel.");
        }

        //--------------------------------------------------------------------------//
        private void GetFinancialEntity(int Offset = 0)
        {
            try
            {
                string FinancialEntity_API = asiBaseAddress + "/api/financialentity?Offset=" + Offset;
                var financialentity_client = new RestClient(FinancialEntity_API);
                var financialentity_request = new RestRequest(Method.GET);

                financialentity_request.AddHeader("cache-control", "no-cache");
                financialentity_request.AddHeader("Content-Type", "application/json");
                financialentity_request.AddHeader("Authorization", "Bearer " + AccessToken);

                IRestResponse financialentity_response = financialentity_client.Execute(financialentity_request);

                if (financialentity_response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    JValue financialentity_value = (JValue)financialentity_response.Content;
                    var financialentity_obj = JObject.Parse(financialentity_value.Value.ToString());
                    JArray financialentity_array = (JArray)financialentity_obj["Items"]["$values"];

                    foreach (var item in financialentity_array)
                    {
                        FinalFinancialEntity_array.Add(item);
                    }

                    bool hasnext = Convert.ToBoolean(financialentity_obj["HasNext"]);

                    if (hasnext)
                    {
                        Offset = Convert.ToInt32(financialentity_obj["NextOffset"]);
                        GetFinancialEntity(Offset);
                    }
                }
                else
                {
                    txtResponse.AppendText("Call Get Financial Entity api error: " + financialentity_response.Content + "\r\n");
                }
            }
            catch (Exception ex)
            {
                txtResponse.AppendText("Call Get Financial Entity api error: " + ex.Message + "\r\n");
            }
        }

        private void GetGLAccount(int Offset = 0)
        {
            try
            {
                string GLAccount_API = asiBaseAddress + "/api/glaccount?Offset=" + Offset;
                var glaccount_client = new RestClient(GLAccount_API);
                var glaccount_request = new RestRequest(Method.GET);

                glaccount_request.AddHeader("cache-control", "no-cache");
                glaccount_request.AddHeader("Content-Type", "application/json");
                glaccount_request.AddHeader("Authorization", "Bearer " + AccessToken);

                IRestResponse glaccount_response = glaccount_client.Execute(glaccount_request);

                if (glaccount_response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    JValue glaccount_value = (JValue)glaccount_response.Content;
                    var glaccount_obj = JObject.Parse(glaccount_value.Value.ToString());
                    JArray glaccount_array = (JArray)glaccount_obj["Items"]["$values"];

                    foreach (var item in glaccount_array)
                    {
                        FinalGLAccount_array.Add(item);
                    }

                    bool hasnext = Convert.ToBoolean(glaccount_obj["HasNext"]);

                    if (hasnext)
                    {
                        Offset = Convert.ToInt32(glaccount_obj["NextOffset"]);
                        GetGLAccount(Offset);
                    }
                }
                else
                {
                    txtResponse.AppendText("Call Get GLAccount api error: " + glaccount_response.Content + "\r\n");
                }
            }
            catch (Exception ex)
            {
                txtResponse.AppendText("Call Get GLAccount api error: " + ex.Message + "\r\n");
            }
        }

        private void GetTaxCategory(int Offset = 0)
        {
            try
            {
                string TaxCategory_API = asiBaseAddress + "/api/taxcategory?Offset=" + Offset;
                var taxcategory_client = new RestClient(TaxCategory_API);
                var taxcategory_request = new RestRequest(Method.GET);

                taxcategory_request.AddHeader("cache-control", "no-cache");
                taxcategory_request.AddHeader("Content-Type", "application/json");
                taxcategory_request.AddHeader("Authorization", "Bearer " + AccessToken);

                IRestResponse taxcategory_response = taxcategory_client.Execute(taxcategory_request);

                if (taxcategory_response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    JValue taxcategory_value = (JValue)taxcategory_response.Content;
                    var taxcategory_obj = JObject.Parse(taxcategory_value.Value.ToString());
                    JArray taxcategory_array = (JArray)taxcategory_obj["Items"]["$values"];

                    foreach (var items in taxcategory_array)
                    {
                        FinalTaxCategory_array.Add(items["Properties"]["$values"]);
                    }

                    bool hasnext = Convert.ToBoolean(taxcategory_obj["HasNext"]);

                    if (hasnext)
                    {
                        Offset = Convert.ToInt32(taxcategory_obj["NextOffset"]);
                        GetTaxCategory(Offset);
                    }
                }
                else
                {
                    txtResponse.AppendText("Call Get TaxCategory api error: " + taxcategory_response.Content + "\r\n");
                }
            }
            catch (Exception ex)
            {
                txtResponse.AppendText("Call Get TaxCategory api error: " + ex.Message + "\r\n");
            }
        }

        private void GetGroup(int Offset = 0)
        {
            try
            {
                string Group_API = asiBaseAddress + "/api/group?Offset=" + Offset;
                var group_client = new RestClient(Group_API);
                var group_request = new RestRequest(Method.GET);

                group_request.AddHeader("cache-control", "no-cache");
                group_request.AddHeader("Content-Type", "application/json");
                group_request.AddHeader("Authorization", "Bearer " + AccessToken);

                IRestResponse group_response = group_client.Execute(group_request);

                if (group_response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    JValue group_value = (JValue)group_response.Content;
                    var group_obj = JObject.Parse(group_value.Value.ToString());
                    JArray group_array = (JArray)group_obj["Items"]["$values"];

                    foreach (var item in group_array)
                    {
                        FinalGroup_array.Add(item);
                    }

                    bool hasnext = Convert.ToBoolean(group_obj["HasNext"]);

                    if (hasnext)
                    {
                        Offset = Convert.ToInt32(group_obj["NextOffset"]);
                        GetGroup(Offset);
                    }
                }
                else
                {
                    txtResponse.AppendText("Call Get Group api error: " + group_response.Content + "\r\n");
                }
            }
            catch (Exception ex)
            {
                txtResponse.AppendText("Call Get Group api error: " + ex.Message + "\r\n");
            }
        }

        private void GetItemClass(int Offset = 0)
        {
            try
            {
                string ItemClass_API = asiBaseAddress + "/api/itemclass?Offset=" + Offset;
                var item_client = new RestClient(ItemClass_API);
                var item_request = new RestRequest(Method.GET);

                item_request.AddHeader("cache-control", "no-cache");
                item_request.AddHeader("Content-Type", "application/json");
                item_request.AddHeader("Authorization", "Bearer " + AccessToken);

                IRestResponse itemclass_response = item_client.Execute(item_request);

                if (itemclass_response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    JValue itemclass_value = (JValue)itemclass_response.Content;
                    var itemclass_obj = JObject.Parse(itemclass_value.Value.ToString());
                    JArray itemclass_array = (JArray)itemclass_obj["Items"]["$values"];

                    foreach (var item in itemclass_array)
                    {
                        FinalItemClass_array.Add(item);
                    }

                    bool hasnext = Convert.ToBoolean(itemclass_obj["HasNext"]);

                    if (hasnext)
                    {
                        Offset = Convert.ToInt32(itemclass_obj["NextOffset"]);
                        GetItemClass(Offset);
                    }
                }
                else
                {
                    txtResponse.AppendText("Call Get ItemClass api error: " + itemclass_response.Content + "\r\n");
                }
            }
            catch (Exception ex)
            {
                txtResponse.AppendText("Call Get ItemClass api error: " + ex.Message + "\r\n");
            }
        }
        //--------------------------------------------------------------------------//
        public class RestResponse
        {
            public bool Status { get; set; }
            public string Message { get; set; }
            public string Response { get; set; }
            public string ErrorCode { get; set; }
        }

        public class TokenResponse
        {
            public string access_token { get; set; }
            public string token_type { get; set; }
            public string expires_in { get; set; }
            public string userName { get; set; }
            public string issued { get; set; }
            public string expires { get; set; }
            public string error { get; set; }
        }
    }
}